//
//  Animations.swift
//  Pablo
//
//  Created by Andrew Mayne on 10/11/15.
//  Copyright © 2015 Red Brick. All rights reserved.
//

import Foundation
import UIKit

class Animations {
    
    class func fadeIn(view: UIView, delay: NSTimeInterval, duration: NSTimeInterval, completion: ((Bool) -> Void)?) {
        UIView.animateWithDuration(duration, delay: delay, options: .CurveEaseIn,
            animations: {
                view.alpha = 1.0
            },
            completion: completion)
    }
    
    class func fadeOut(view: UIView, delay: NSTimeInterval, duration: NSTimeInterval, completion: ((Bool) -> Void)?) {
        UIView.animateWithDuration(duration, delay: delay, options: .CurveEaseOut,
            animations: {
                view.alpha = 0.0
            },
            completion: completion)
    }
}