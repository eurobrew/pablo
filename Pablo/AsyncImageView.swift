//
//  AsyncImageView.swift
//  Pablo
//
//  Created by Andrew Mayne on 10/12/15.
//  Copyright © 2015 Red Brick. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

class AsyncImageView: UIView {

    let fadeDuration = 1.0
    let delayRange = (100...1000)
    
    let image: UIImageView
    let placeholder: UIView = AsyncImageView.buildPlaceholderView()
    let primaryColor: UIColor
    
    init(image: UIImage) {
        self.image = UIImageView(image: image)
        self.primaryColor = image.averageColor()
        super.init(frame: CGRectZero)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup() {
        image.contentMode = .ScaleAspectFill
        placeholder.backgroundColor = primaryColor
        
        addSubview(placeholder)
        placeholder.snp_makeConstraints(closure: constraints())
        
        self.image.alpha = 0
        self.addSubview(self.image)
        self.image.snp_makeConstraints(closure: self.constraints())

        Animations.fadeIn(image, delay: randomDelay(), duration: fadeDuration) { (_) -> Void in
            self.placeholder.removeFromSuperview()
        }
    }
    
    private func randomDelay() -> Double {
        return Double(delayRange.random)/1000.0
    }
    
    private func constraints() -> ((ConstraintMaker) -> Void) {
        return { (make) -> Void in
            make.height.equalTo(self)
            make.width.equalTo(self)
            make.center.equalTo(self)
        }
    }
    
    private class func buildPlaceholderView() -> UIView {
        let placeholder = UIView(frame: CGRectZero)
        placeholder.backgroundColor = UIColor.whiteColor()
        return placeholder
    }
}