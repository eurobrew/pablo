//
//  UIBezierPath+Circles.swift
//  Pablo
//
//  Created by Andrew Mayne on 10/11/15.
//  Copyright © 2015 Red Brick. All rights reserved.
//

import Foundation
import UIKit

extension CAShapeLayer {
    
    class func circle(size: CGFloat) -> CAShapeLayer {
        let circle = CAShapeLayer()
        
        // Make a circular shape
        let circularPath = UIBezierPath(roundedRect: CGRectMake(0, 0, size, size), cornerRadius: size)
        
        circle.path = circularPath.CGPath
        return circle
    }
    
    class func circleOutline(size: CGFloat, lineWidth: CGFloat, stroke: UIColor, fill: UIColor) -> CAShapeLayer {
        let center = size/2-2*lineWidth
        let circle = CAShapeLayer.circle(size)
        circle.bounds = CGRectMake(0, 0, center, center)
        circle.position = CGPointMake(0, 0)
        circle.strokeColor = stroke.CGColor
        circle.fillColor = fill.CGColor
        circle.lineWidth = lineWidth
        return circle
    }
    
}