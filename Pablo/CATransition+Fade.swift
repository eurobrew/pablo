//
//  CATransition+Fade.swift
//  Pablo
//
//  Created by Andrew Mayne on 10/12/15.
//  Copyright © 2015 Red Brick. All rights reserved.
//

import Foundation
import UIKit

extension CATransition {
    
    class func fadeIn(duration: CFTimeInterval) -> CATransition {
        let transition = CATransition()
        transition.duration = duration
        transition.type = kCATransitionFade
        transition.subtype = kCATransitionFromTop
        return transition
    }
}