//
//  DottedLineView.swift
//  Pablo
//
//  Created by Andrew Mayne on 10/11/15.
//  Copyright © 2015 Red Brick. All rights reserved.
//

import Foundation
import UIKit

class DottedLineView {
    
    class func create(width: CGFloat, stroke: UIColor) -> UIImageView {
        let offset = CGFloat(2)
        let path = UIBezierPath()
        
        path.moveToPoint(CGPointMake(offset, 10))
        path.addLineToPoint(CGPointMake(width - offset, 10))
        path.lineWidth = 4
        
        let dashes: [CGFloat] = [ path.lineWidth * 0, path.lineWidth * 2 ]
        path.setLineDash(dashes, count: dashes.count, phase: 0)
        path.lineCapStyle = .Round
        
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(width, 20), false, 2)
        stroke.setStroke()
        path.stroke()
        let dottedLine = UIImageView(image: UIGraphicsGetImageFromCurrentImageContext())
        UIGraphicsEndImageContext()
        
        return dottedLine
    }
}