//
//  Pablo.swift
//  Pablo
//
//  Created by Andrew Mayne on 10/11/15.
//  Copyright © 2015 Red Brick. All rights reserved.
//

import Foundation
import UIKit

class Pablo {
    
    let image: UIImage
    let year: Int
    let title: String
    let description: String
    
    init(image: UIImage, year: Int, title: String, description: String) {
        self.image = image
        self.year = year
        self.title = title
        self.description = description
    }
    
    convenience init(image: UIImage, year: Int, title: String) {
        self.init(image: image, year: year, title: title, description: String(year))
    }
}