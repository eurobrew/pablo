//
//  PictureCollectionViewCell.swift
//  Pablo
//
//  Created by Andrew Mayne on 10/11/15.
//  Copyright © 2015 Red Brick. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

class PabloCollectionViewCell: UICollectionViewCell {

    var pablo: Pablo? = nil
    var dottedLine: UIImageView?
    var imageView: AsyncImageView?
    
    func setup(pablo: Pablo, index: Int, total: Int) {
        self.pablo = pablo
        
        setupDottedLine(index, total: total)
        
        imageView = AsyncImageView(image: pablo.image)
        
        contentView.addSubview(imageView!)

        imageView!.snp_makeConstraints { (make) -> Void in
            make.height.equalTo(contentView)
            make.width.equalTo(contentView.snp_height)
            make.center.equalTo(contentView.snp_center)
        }
        
        imageView!.layer.mask = CAShapeLayer.circle(contentView.bounds.height)
    }
    
    // inspired from http://stackoverflow.com/questions/26018302/draw-dotted-not-dashed-line
    func setupDottedLine(index: Int, total: Int) {
        if total <= 1 {
            return
        }
        
        let strokeColor: UIColor = UIColor.lightBlackColor()
        
        if isLeftSidedOnly(index, total: total) || isRightSidedOnly(index, total: total) {
            dottedLine = DottedLineView.create(UIScreen.mainScreen().bounds.size.width/2, stroke: strokeColor)
        } else {
            dottedLine = DottedLineView.create(UIScreen.mainScreen().bounds.size.width, stroke: strokeColor)
        }
        
        contentView.addSubview(dottedLine!)
        
        dottedLine!.snp_makeConstraints { (make) -> Void in
            if isRightSidedOnly(index, total: total) {
                make.left.equalTo(contentView.snp_centerX)
                make.centerY.equalTo(contentView.snp_centerY)
            } else if isLeftSidedOnly(index, total: total) {
                make.left.equalTo(contentView)
                make.centerY.equalTo(contentView.snp_centerY)
            } else {
                make.center.equalTo(contentView)
            }
        }
    }
    
    private func isRightSidedOnly(index: Int, total: Int) -> Bool {
        return index == 0 && total > 1
    }
    
    private func isLeftSidedOnly(index: Int, total: Int) -> Bool {
        return index == total - 1
    }
    
    override func prepareForReuse() {
        for v in contentView.subviews {
            v.removeFromSuperview()
        }
    }
}