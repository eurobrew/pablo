//
//  PictureCollectionViewController.swift
//  Pablo
//
//  Created by Andrew Mayne on 10/11/15.
//  Copyright © 2015 Red Brick. All rights reserved.
//

import Foundation
import UIKit

class PabloCollectionViewController: UIViewController, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    let textFadeInDuration = 0.2
    let textFadeOutDuration = 0.2
    
    var collectionView: UICollectionView?
    let pablos: [Pablo]
    let layout = PabloViewLayout()
    let titleLabel = UILabel()
    let descriptionLabel = UILabel()
    
    init(pablos: [Pablo]) {
        self.pablos = pablos
        super.init(nibName: nil, bundle: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
        setupLogo()
        setupTitle()
        setupDescription()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        configureLabelForVisibleCell()
    }
    
    func setupCollectionView() {
        let collectionView = UICollectionView(frame: view.frame, collectionViewLayout: layout)

        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.registerClass(PabloCollectionViewCell.self, forCellWithReuseIdentifier: "Cell")
        collectionView.backgroundColor = .clearColor()
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        
        view.addSubview(collectionView)
        
        self.collectionView = collectionView
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return pablos.count
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as! PabloCollectionViewCell
        
        let index = indexPath.section
        
        cell.setup(pablos[index], index: indexPath.section, total: pablos.count)
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let cell: PabloCollectionViewCell = collectionView.cellForItemAtIndexPath(indexPath) as! PabloCollectionViewCell

        navigationController?.view.layer.addAnimation(CATransition.fadeIn(0.6), forKey: nil)
        navigationController?.pushViewController(PabloPictureViewController(image: cell.pablo!.image), animated: false)
    }
    
    func setupLogo() {
        let logo = UIImageView(image: UIImage(named: "Logo"))
        
        view.addSubview(logo)
        
        logo.snp_makeConstraints { (make) -> Void in
            make.topMargin.equalTo(60)
            make.centerX.equalTo(view)
        }
    }
    
    func setupTitle() {
        titleLabel.font = .headerFont(20)
        titleLabel.textAlignment = .Center
        titleLabel.text = ""
        
        view.addSubview(titleLabel)
        
        titleLabel.snp_makeConstraints { (make) -> Void in
            make.bottomMargin.equalTo(-80)
            make.centerX.equalTo(view)
            make.width.equalTo(view)
        }
    }
    
    func setupDescription() {
        descriptionLabel.font = .headerFont(14)
        descriptionLabel.textAlignment = .Center
        descriptionLabel.text = ""
        
        view.addSubview(descriptionLabel)
        
        descriptionLabel.snp_makeConstraints { (make) -> Void in
            make.top.equalTo(titleLabel.snp_bottom)
            make.centerX.equalTo(view)
            make.width.equalTo(200)
        }
    }
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        configureLabelForVisibleCell()
    }
    
    func scrollViewWillBeginDragging(scrollView: UIScrollView) {
        updateTitleAndDescription(nil)
    }
    
    func configureLabelForVisibleCell() {
        let visibleRect = CGRectMake(collectionView!.contentOffset.x, collectionView!.contentOffset.y, collectionView!.bounds.width, collectionView!.bounds.height)
        let visiblePoint = CGPointMake(CGRectGetMidX(visibleRect), CGRectGetMidY(visibleRect))
        if let visibleIndexPath = collectionView?.indexPathForItemAtPoint(visiblePoint) {
            if let cell = collectionView?.cellForItemAtIndexPath(visibleIndexPath) as? PabloCollectionViewCell {
                updateTitleAndDescription(cell.pablo)
            }
        }
    }
    
    func updateTitleAndDescription(pablo: Pablo?) {
        if let p = pablo {
            titleLabel.text = p.title
            descriptionLabel.text = p.description
            // Fade in
            Animations.fadeIn(titleLabel, delay: 0.0, duration: textFadeInDuration, completion: nil)
            Animations.fadeIn(descriptionLabel, delay: 0.0, duration: textFadeInDuration, completion: nil)
        } else {
            Animations.fadeOut(titleLabel, delay: 0.0, duration: textFadeOutDuration, completion: { (finished: Bool) -> Void in
                self.titleLabel.text = nil
            })
            Animations.fadeOut(descriptionLabel, delay: 0.0, duration: textFadeOutDuration, completion: { (finished: Bool) -> Void in
                self.descriptionLabel.text = nil
            })
        }
    }
}