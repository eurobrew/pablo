//
//  PabloPictureViewController.swift
//  Pablo
//
//  Created by Andrew Mayne on 10/12/15.
//  Copyright © 2015 Red Brick. All rights reserved.
//

import Foundation
import UIKit

class PabloPictureViewController: UIViewController {
    
    let image: UIImage
    var imageView: UIImageView?
    var dismissButton: UIButton?
    
    init(image: UIImage) {
        self.image = image
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidAppear(animated: Bool) {
        
        imageView = UIImageView(image: image)
        imageView!.contentMode = .ScaleAspectFill
        
        view.addSubview(imageView!)
        imageView!.snp_makeConstraints { (make) -> Void in
            make.height.equalTo(view)
            make.width.equalTo(view)
        }
        
        dismissButton = createDismissButton()
        
        view.addSubview(dismissButton!)
        dismissButton!.snp_makeConstraints { (make) -> Void in
            make.centerX.equalTo(view)
            make.bottomMargin.equalTo(-40)
        }
    }
    
    func createDismissButton() -> UIButton {
        let dismissButton = UIButton(type: .Custom)
        dismissButton.setBackgroundImage(UIImage(named: "Dismiss"), forState: UIControlState.Normal)
        dismissButton.addTarget(self, action: "dismiss", forControlEvents: UIControlEvents.TouchUpInside)
        dismissButton.backgroundColor = UIColor.clearColor()
        
        let stroke = UIColor.whiteColor().colorWithAlphaComponent(0.8)
        let fill = UIColor.blackColor().colorWithAlphaComponent(0.1)
        let circleOutline = CAShapeLayer.circleOutline(40, lineWidth: 1.5, stroke: stroke, fill: fill)
        dismissButton.layer.addSublayer(circleOutline)
        
        return dismissButton
    }
    
    func dismiss() {
        navigationController?.view.layer.addAnimation(CATransition.fadeIn(0.3), forKey: nil)
        navigationController?.popViewControllerAnimated(false)
    }
}