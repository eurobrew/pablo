//
//  PictureViewLayout.swift
//  Pablo
//
//  Created by Andrew Mayne on 10/11/15.
//  Copyright © 2015 Red Brick. All rights reserved.
//

import Foundation
import UIKit

class PabloViewLayout: UICollectionViewFlowLayout {
    
    override func prepareLayout() {
        super.prepareLayout()
        setupLayout(collectionView!.bounds)
    }
    
    func setupLayout(collectionViewBounds: CGRect) {
        let width = collectionViewBounds.size.width
        let height = collectionViewBounds.size.height
        let inset  = floor(width * (1/4))
        
        let size = width - (2 * inset)
        let insetY = height/2 - size
        
        itemSize = CGSizeMake(width, size)
        sectionInset = UIEdgeInsetsMake(insetY, 0, insetY, 0)
        scrollDirection = .Horizontal
    }
    
    // snap to cell - taken from https://gist.github.com/mmick66/9812223
    override func targetContentOffsetForProposedContentOffset(proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {
        let collectionViewSize = collectionView!.bounds.size
        let proposedContentOffsetCenterX = proposedContentOffset.x + collectionViewSize.width * 0.5
        
        let proposedRect = collectionView!.bounds
        
        // Comment out if you want the collectionview simply stop at the center of an item while scrolling freely
        // proposedRect = CGRectMake(proposedContentOffset.x, 0.0, collectionViewSize.width, collectionViewSize.height);
        
        var candidateAttributes: UICollectionViewLayoutAttributes? = nil
        if let layoutAttributes = layoutAttributesForElementsInRect(proposedRect) {
            for attributes in layoutAttributes  {
                // == Skip comparison with non-cell items (headers and footers) == //
                if (attributes.representedElementCategory != .Cell)
                {
                    continue;
                }
                
                // == First time in the loop == //
                if candidateAttributes == nil
                {
                    candidateAttributes = attributes;
                    continue;
                }
                
                if (fabsf(Float(attributes.center.x - proposedContentOffsetCenterX)) < fabsf(Float(candidateAttributes!.center.x - proposedContentOffsetCenterX)))
                {
                    candidateAttributes = attributes;
                }
            }
        }
        
        return CGPointMake(candidateAttributes!.center.x - collectionViewSize.width * 0.5, proposedContentOffset.y)
    }
}