//
//  Range+Random.swift
//  Pablo
//
//  Created by Andrew Mayne on 10/12/15.
//  Copyright © 2015 Red Brick. All rights reserved.
//

import Foundation

// from http://stackoverflow.com/questions/24132399/how-does-one-make-random-number-between-range-for-arc4random-uniform
extension Range
{
    var random: Int
        {
        get
        {
            var offset = 0
            
            if (startIndex as! Int) < 0   // allow negative ranges
            {
                offset = abs(startIndex as! Int)
            }
            
            let mini = UInt32(startIndex as! Int + offset)
            let maxi = UInt32(endIndex   as! Int + offset)
            
            return Int(mini + arc4random_uniform(maxi - mini)) - offset
        }
    }
}
