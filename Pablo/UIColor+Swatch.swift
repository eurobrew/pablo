//
//  UIColorExtensions.swift
//  Pablo
//
//  Created by Andrew Mayne on 10/11/15.
//  Copyright © 2015 Red Brick. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    public class func lightBlackColor() -> UIColor {
        return UIColor(rgba: "#4c4c4c")
    }
}