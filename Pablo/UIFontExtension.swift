//
//  UIFontExtension.swift
//  Pablo
//
//  Created by Andrew Mayne on 10/11/15.
//  Copyright © 2015 Red Brick. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {
    
    class func headerFont(size: CGFloat) -> UIFont {
        return UIFont(name: "Zapfino", size: size)!
    }
}