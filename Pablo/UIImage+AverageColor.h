/*
 UIImage+AverageColor.h
 */

// taken from http://www.bobbygeorgescu.com/2011/08/finding-average-color-of-uiimage/

#import <UIKit/UIKit.h>

@interface UIImage (AverageColor)
- (UIColor *)averageColor;
@end