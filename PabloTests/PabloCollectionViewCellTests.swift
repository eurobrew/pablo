//
//  PabloCollectionViewCellTests.swift
//  Pablo
//
//  Created by Andrew Mayne on 10/12/15.
//  Copyright © 2015 Red Brick. All rights reserved.
//

import XCTest
@testable import Pablo

class PabloCollectionViewCellTests: XCTestCase {
    
    let pablo = Pablo(image: UIImage(), year: 2015, title: "")
    
    func testOneCell() {
        let subject = PabloCollectionViewCell()
        
        XCTAssertNil(subject.pablo)
        XCTAssertNil(subject.dottedLine)
        XCTAssertNil(subject.imageView)
        
        subject.setup(pablo, index: 0, total: 1)

        XCTAssertEqual(subject.contentView.subviews.count, 1)
        XCTAssertNil(subject.dottedLine)
    }
    
    func testLeftMostCell() {
        let subject = PabloCollectionViewCell()
        
        XCTAssertNil(subject.pablo)
        XCTAssertNil(subject.dottedLine)
        XCTAssertNil(subject.imageView)
        
        subject.setup(pablo, index: 0, total: 2)
        
        XCTAssertEqual(subject.contentView.subviews.count, 2)
        XCTAssertEqual(subject.dottedLine!.frame.width, UIScreen.mainScreen().bounds.width/2)
    }
    
    func testRightMostCell() {
        let subject = PabloCollectionViewCell()
        
        XCTAssertNil(subject.pablo)
        XCTAssertNil(subject.dottedLine)
        XCTAssertNil(subject.imageView)
        
        subject.setup(pablo, index: 1, total: 2)
        
        XCTAssertEqual(subject.contentView.subviews.count, 2)
        XCTAssertEqual(subject.dottedLine!.frame.width, UIScreen.mainScreen().bounds.width/2)
    }
    
    func testMiddleCell() {
        let subject = PabloCollectionViewCell()
        
        XCTAssertNil(subject.pablo)
        XCTAssertNil(subject.dottedLine)
        XCTAssertNil(subject.imageView)
        
        subject.setup(pablo, index: 1, total: 3)
        
        XCTAssertEqual(subject.contentView.subviews.count, 2)
        XCTAssertEqual(subject.dottedLine!.frame.width, UIScreen.mainScreen().bounds.width)
    }
}
