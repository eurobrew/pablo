//
//  PabloCollectionViewCellTests.swift
//  Pablo
//
//  Created by Andrew Mayne on 10/12/15.
//  Copyright © 2015 Red Brick. All rights reserved.
//

import XCTest
@testable import Pablo

class PabloCollectionViewControllerTests: XCTestCase {
    
    let pablos = [Pablo(image: UIImage(), year: 2015, title: "Test1"),Pablo(image: UIImage(), year: 2016, title: "Test2")]
    
    func testViewDidLoad() {
        let subject = PabloCollectionViewController(pablos: pablos)
        
        XCTAssertNil(subject.collectionView)
        
        subject.viewDidLoad()
        
        XCTAssertEqual(subject.view.subviews.count, 6)
        XCTAssertNotNil(subject.titleLabel.text)
        XCTAssertNotNil(subject.descriptionLabel.text)
    }
    
    func testSetupCollectionView() {
        let subject = PabloCollectionViewController(pablos: pablos)
        
        XCTAssertNil(subject.collectionView)
        
        subject.setupCollectionView()
        
        let collectionView = subject.collectionView!
        XCTAssertEqual(collectionView.backgroundColor, UIColor.clearColor())
        XCTAssertEqual(collectionView.showsHorizontalScrollIndicator, false)
        XCTAssertEqual(collectionView.showsVerticalScrollIndicator, false)
        XCTAssertNotNil(collectionView.delegate)
        XCTAssertNotNil(collectionView.dataSource)
    }
    
    func testNumberOfSectionsInCollectionView() {
        let subject = PabloCollectionViewController(pablos: pablos)
        
        subject.setupCollectionView()
        
        XCTAssertEqual(subject.numberOfSectionsInCollectionView(subject.collectionView!), 2)
        XCTAssertEqual(subject.collectionView(subject.collectionView!, numberOfItemsInSection: 0), 1)
        XCTAssertEqual(subject.collectionView(subject.collectionView!, numberOfItemsInSection: 1), 1)
    }
    
    func testUpdateTitleAndDescription() {
        let subject = PabloCollectionViewController(pablos: pablos)
        
        subject.updateTitleAndDescription(pablos[1])
        
        XCTAssertEqual(subject.titleLabel.text!, "Test2")
        XCTAssertEqual(subject.descriptionLabel.text!, "2016")
    }
}
