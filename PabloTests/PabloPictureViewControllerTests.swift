//
//  PabloPictureViewControllerTests.swift
//  Pablo
//
//  Created by Andrew Mayne on 10/12/15.
//  Copyright © 2015 Red Brick. All rights reserved.
//

import XCTest
import UIKit
@testable import Pablo

class PabloPictureViewControllerTests: XCTestCase {
    
    func testViewDidAppear() {
        let subject = PabloPictureViewController(image: UIImage())
        
        XCTAssertNil(subject.dismissButton)
        XCTAssertNil(subject.imageView)
        
        subject.viewDidAppear(true)
        
        XCTAssertEqual(subject.dismissButton!.allTargets().count, 1)
        XCTAssertEqual(subject.dismissButton!.backgroundColor, UIColor.clearColor())
        XCTAssertEqual(subject.dismissButton!.layer.sublayers!.count, 1)
        XCTAssertEqual(subject.imageView!.contentMode, UIViewContentMode.ScaleAspectFill)
    }
}
