//
//  PabloViewLayoutTests.swift
//  Pablo
//
//  Created by Andrew Mayne on 10/12/15.
//  Copyright © 2015 Red Brick. All rights reserved.
//

import XCTest
@testable import Pablo

class PabloViewLayoutTests: XCTestCase {
    
    func testSetupLayout() {
        let subject = PabloViewLayout()
        
        subject.setupLayout(CGRectMake(0, 0, 100, 100))
        
        XCTAssertEqual(subject.itemSize, CGSizeMake(100, 50))
        XCTAssertEqual(subject.scrollDirection, UICollectionViewScrollDirection.Horizontal)
    }
}
